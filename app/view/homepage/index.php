<!DOCTYPE html>
<html>

<head lang="en">
    <meta charset="UTF-8">
    <title>Freezebox</title>

    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.11.1/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/3.32/jquery.form.js"></script>
    <!--Royal Slider-->
    <link rel="stylesheet" href="app/assets/vendors/royalslider/royalslider.css">
    <link rel="stylesheet" href="app/assets/vendors/royalslider/skins/default/rs-default.css">
    <link rel="stylesheet" href="app/assets/vendors/royalslider/skins/minimal-white/rs-minimal-white.css">

<!--    <script src='app/assets/vendors/royalslider/jquery-1.8.3.min.js'></script>-->
    <script src="app/assets/vendors/royalslider/jquery.royalslider.min.js"></script>

    <!--END-->

    <script src="//use.typekit.net/uff2tsn.js"></script>
    <script>
        try {
            Typekit.load();
        } catch (e) {
        }
    </script>


    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link href="app/assets/css/main.css" rel="stylesheet" type="text/css"/>
</head>

<body>
<header>
    <div class="wrapper">
        <div class="logo">
            <a href="index.html"><img src="app/assets/img/logo.png" alt=""></a>
        </div>

        <nav>
            <ul>
                <li class="btn green inquiry">
                    <a href="#contact">Send inquiry</a>
                </li>

                <li class="btn">
                    <a href="#">HR</a>
                </li>

                <li class="btn active">
                    <a href="#">ENG</a>
                </li>
            </ul>
        </nav>
    </div>

    <div class="slider-header rsDefault">
        <div class="rsContent slide01">
            <div class="wrapper">
                <h1>Neki slogan koji ide preko cijelog ekrana</h1>

                <div class="btn btn-getFreezebox">
                    <a href="#">Get your freezebox</a>
                </div>
            </div>
        </div>

        <div class="rsContent slide02">
            <div class="wrapper">
                <h1>Neki drugi slogan koji takoder ide preko cijelog ekrana</h1>

                <div class="btn btn-getFreezebox">
                    <a href="#">Get your freezebox</a>
                </div>
            </div>
        </div>
    </div>
</header>

<div class="content">
    <section class="desc">
        <div class="wrapper">
            <div class="short">Looking for something to spice up your special event and make it even more fun and
                exciting for your guests? From weddings to club events, birthday parties or corporate events, Freezebox
                is an absolute <em>'must have'</em> on anybody's entertainment list. Our portable and elegant photo
                booth is a guarantee of extremely fun and interactive photo experience at your party and being so small
                and light, it can be put and transferred anywhere you need it.
            </div>

            <div class="long">Freezebox is equipped with a <em>professional high definition cameras, touch screen
                    technology, professional studio lighting, social media sharing</em> options and tons of interesting
                props that will make your <em>Freezebox</em> experience unforgettable. And to make sure your guests
                bring home a fun and special memory, there’s a printer specialised for fast printing high quality photos
                straight from Freezebox right after they’re taken!

                <br><br>By now you've realised that <em>Freezebox</em> and parties are a perfect match, so without
                further ado, <em>let's get your party started!</em>
            </div>
        </div>
    </section>

    <section class="how">
        <div class="wrapper full">
            <h2>How does Freezebox work?</h2>

            <div class="block">
                <div class="img">
                    <img src="app/assets/img/pic_01.jpg" alt="">
                </div>

                <div class="text-group">
                    <div class="container">
                        <div class="num">1</div>

                        <div class="desc">Make a pose with your loved ones!</div>
                    </div>
                </div>
            </div>

            <div class="block">
                <div class="img">
                    <img src="app/assets/img/pic_01.jpg" alt="">
                </div>

                <div class="text-group">
                    <div class="container">
                        <div class="num">2</div>

                        <div class="desc">Click on a button and 3,2,1…</div>
                    </div>
                </div>
            </div>

            <div class="block">
                <div class="img">
                    <img src="app/assets/img/pic_01.jpg" alt="">
                </div>

                <div class="text-group">
                    <div class="container">
                        <div class="num">3</div>

                        <div class="desc">Your photo is instantly shared with the rest of the world! Cool!</div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="moments">
        <div class="wrapper">
            <h2>Captured moments</h2>

            <div class="container">
                <div class="frame"></div>

                <div class="slider-moments rsDefault">
                    <div class="rsContent">
                        <img src="app/assets/img/moment_01.jpg" alt="">
                    </div>

                    <div class="rsContent">
                        <img src="app/assets/img/moment_01.jpg" alt="">
                    </div>

                    <div class="rsContent">
                        <img src="app/assets/img/moment_01.jpg" alt="">
                    </div>

                    <div class="rsContent">
                        <img src="app/assets/img/moment_01.jpg" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="testimonials">
        <div class="wrapper">
            <h2>Testimonials</h2>

            <div class="slider-testimonials rsDefault">
                <div class="rsContent">
                    <div class="img">
                        <img src="app/assets/img/thumb_01.png" alt="">
                    </div>

                    <div class="quote">People thank you very much ... We all preoduševljeni ... Freezebox is a HIT !!!!
                        All who had painted happy they are, and those who are not now call and complain ... So all
                        future users Take it while you let the flash ... :) Thank you from the heart! Love you all!!!
                    </div>

                    <div class="by">Toma & Martina</div>
                </div>

                <div class="rsContent">
                    <div class="img">
                        <img src="app/assets/img/thumb_01.png" alt="">
                    </div>

                    <div class="quote">Duis tincidunt condimentum libero. Curabitur dignissim rutrum pellentesque.
                        Mauris convallis risus odio, non vulputate leo sagittis eget. Class aptent taciti sociosqu ad
                        litora torquent per conubia nostra, per inceptos himenaeos. Sed id nisi vitae nibh hendrerit
                        pharetra. Praesent nec mollis lectus. Suspendisse risus velit, bibendum sed ultricies nec,
                        aliquet et diam. Cras at libero sollicitudin, dignissim mi sed, gravida erat. In lacinia
                        pharetra consectetur. Curabitur consequat porttitor luctus.
                    </div>

                    <div class="by">Toma & Martina</div>
                </div>

                <div class="rsContent">
                    <div class="img">
                        <img src="app/assets/img/thumb_01.png" alt="">
                    </div>

                    <div class="quote">Aenean euismod mi elit, non pellentesque metus posuere eget. Curabitur venenatis
                        tortor id risus finibus lobortis. Vivamus id scelerisque ex. Vestibulum nec auctor neque.
                    </div>

                    <div class="by">Toma & Martina</div>
                </div>
            </div>
        </div>
    </section>

    <section id="contact" class="contact">
        <div class="wrapper">
            <h2>Contact us</h2>

            <form name="contact" method="POST" novalidate="novalidate" id="contactForm" action="/contact">
                <div class="left">
                    <div class="input-group ">
                        <input type="text" placeholder="Full name"  id="name" name="name" required>
                        <i class="fa fa-check-circle"></i>
                    </div>

                    <div class="input-group ">
                        <input type="text" placeholder="Phone number" value="0981770314" id="phone" name="phone">
                        <i class="fa fa-check-circle"></i>
                    </div>

                    <div class="input-group ">
                        <input type="email" placeholder="E-mail" id="email" name="email" required>
                        <i class="fa fa-check-circle"></i>
                    </div>

                    <div class="input-group">
                        <input type="text" placeholder="Type of event" id="event" name="event">
                        <i class="fa fa-check-circle"></i>
                    </div>
                </div>

                <div class="right">
                    <div class="input-group">
                        <input type="text" placeholder="Event date" id="date" name="date">
                        <i class="fa fa-check-circle"></i>
                    </div>

                    <div class="input-group">
                        <input type="text" placeholder="Event place" id="place" name="place">
                        <i class="fa fa-check-circle "></i>
                    </div>

                    <div class="input-group">
                        <textarea placeholder="Note" name="note"></textarea>
                        <i class="fa fa-check-circle"></i>
                    </div>
                </div>

                <div id="alertMessage" class="alert">Thank you for your interest.</div>

                <button type="submit" class="btn green btn-send" id="submit" name="submit">Send</button>
            </form>
            <?php $this->renderFeedback();?>
        </div>
    </section>
</div>

<footer>
    <div class="wrapper">
        <div class="social">
            <div class="brand">
                <a href="#">
                    <i class="fa fa-facebook"></i>
                    <span>Facebook</span>
                </a>
            </div>

            <div class="brand">
                <a href="#">
                    <i class="fa fa-instagram"></i>
                    <span>Instagram</span>
                </a>
            </div>

            <div class="brand">
                <a href="#">
                    <i class="fa fa-pinterest"></i>
                    <span>Pintrest</span>
                </a>
            </div>
        </div>

        <div class="statements">
            <span>All rights reserved © 2015, Freezebox</span>
            <span>Site by <a href="http://www.locastic.com">Locastic</a></span>
        </div>
    </div>
</footer>

<script>
    jQuery(document).ready(function ($) {
        $(".slider-header").royalSlider({
            arrowsNav: false,
            loop: true,
            fadeinLoadedSlide: true,
            transitionType: 'fade',
            transitionSpeed: 400,
            controlNavigation: 'none',
            autoplay: {
                enabled: true,
                delay: 5000
            }
        });

        $(".slider-moments").royalSlider({
            arrowsNav: false,
            loop: true,
            transitionSpeed: 400,
            controlNavigation: 'bullets',
            autoplay: {
                enabled: true,
                delay: 8000
            }
        });

        $(".slider-testimonials").royalSlider({
            arrowsNav: false,
            loop: true,
            transitionSpeed: 400,
            controlNavigation: 'none',
            autoHeight: true,
            autoplay: {
                enabled: true,
                delay: 8000
            }
        });
    });

    $(window).load(function () {
        var h = $(window).height();
        var header = $(".slider-header");
        header.css('height', h);
        $('.rsOverflow', header).css('height', h);
        $('.wrapper', header).css('height', h);

        $('.rsContent', header).append("<div class='overlay'></div>");
    });

    $(window).resize(function () {
        var h = $(window).height();
        var header = $(".slider-header");
        header.css('height', h);
        $('.rsOverflow', header).css('height', h);
        $('.wrapper', header).css('height', h);
    });

    $(window).resize(function () {
        var container = $('section.moments .container');
        var slider = $('section.moments .container .slider-moments');
        var scalar = $(container).width() / 956;
        container.css('height', scalar * 649 + 40);
        slider.css('padding-top', scalar * 48);
        slider.css('margin-left', scalar * 62);
        slider.css('width', scalar * 830);
        slider.css('height', scalar * 550);
        $('img', slider).css('width', scalar * 830);
        $('img', slider).css('height', scalar * 550);
    });

    $(window).load(function () {
        $(window).trigger('resize');
    });

    $(function() {
        $('#contactForm').validate({
            rules: {
                name: {
                    required: true,
                    minlength: 2,
                    maxlength: 40
                },
                email: {
                    required: true,
                    email: true
                },
                note:{
                    required: true,
                    minlength: 10,
                    maxlength: 200
                },
                event:{
                    required: true
                },
                phone:{
                    required: true
                },
                date:{
                    required: true
                },
                place:{
                    required: true
                }
            },
            highlight: function (element) {
                $(element).closest('.input-group').removeClass('valid').addClass('invalid');
                $(element).removeClass('success').addClass('error');
            },
            unhighlight: function (element) {
                $(element).closest('.input-group').removeClass('invalid').addClass('valid');
                $(element).removeClass('error').addClass('success');
                $('label').remove();
            },
            success: function(element) {
                element.closest('.input-group').removeClass('invalid').addClass('valid');
            },
            submitHandler: function(form) {
                $(form).ajaxSubmit({
                    type:"POST",
                    data: $(form).serialize(),
                    url:"contact",
                    success: function() {
                        $('#contact :input').attr('disabled', 'disabled');
                        $('#contact').fadeTo( "slow", 0.15, function() {
                            $(this).find(':input').attr('disabled', 'disabled');
                            $(this).find('label').css('cursor','default');
                            $('#alertMessage').fadeIn();
                        });
                    },
                    error: function() {
                        alert('radi!!!!');s
                            $('#contact').fadeTo( "slow", 0.15, function() {
                                $('#error').fadeIn();
                            });
                            $("label").remove();

                    }
                });
            }
        });
    });

</script>
</body>
</html>
