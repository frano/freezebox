<?php
/**
 * Created by PhpStorm.
 * User: frano
 * Date: 22.03.15.
 * Time: 18:38
 */

class Contact extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $contactModel = $this->loadModel('ContactModel');
        $contactSent = $contactModel->sendContact();

        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) and ($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' and $contactSent) {
            return true;
        }else{
            $this->view->render('homepage/index');
        }
    }
}