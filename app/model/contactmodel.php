<?php
/**
 * Created by PhpStorm.
 * User: frano
 * Date: 24.03.15.
 * Time: 12:03
 */

class ContactModel
{
    private $name;
    private $telNumber;
    private $email;
    private $typeOfEvent;
    private $eventDate;
    private $eventPlace;
    private $note;

    public function sendContact()
    {
        if (empty($_POST['name'])) {
            $_SESSION['feedback_negative'][] = "Name field is empty or doesn't fit pattern";
        } elseif (empty($_POST['phone'])) {
            $_SESSION['feedback_negative'][] = "Password field is empty";
        } elseif (empty($_POST['event'])) {
            $_SESSION['feedback_negative'][] = "Event type field is empty";
        } elseif (empty($_POST['date'])) {
            $_SESSION['feedback_negative'][] = "Event date field is empty";
        } elseif (empty($_POST['place'])) {
            $_SESSION['feedback_negative'][] = "Event place field is empty";
        } elseif (empty($_POST['note'])) {
            $_SESSION['feedback_negative'][] = "Note field is empty";
        } elseif (empty($_POST['email'])) {
            $_SESSION['feedback_negative'][] = "Email field is empty!";
        } elseif (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
            $_SESSION['feedback_negative'][] = "Entered email doesn't fit email pattern";
        } elseif(!empty($_POST['name'])
            and preg_match('/^[a-z\d]{2,40}$/i', $_POST['name'])
            and !empty($_POST['phone'])
            and !empty($_POST['email'])
            and filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)
            and !empty($_POST['event'])
            and !empty($_POST['date'])
            and !empty($_POST['note'])
            and preg_match('~(?=.*[0-9])(?=.*[a-z])^[a-z0-9]{10, 200}$~', $_POST['note'])
            and !empty($_POST['place'])
        )
        {
            $this->name = strip_tags($_POST['name']);
            $this->telNumber = strip_tags($_POST['phone']);
            $this->email = strip_tags($_POST['email']);
            $this->note = strip_tags($_POST['note']);
            $this->eventPlace = strip_tags($_POST['place']);
            $this->eventDate = strip_tags($_POST['date']);
            $this->typeOfEvent = strip_tags($_POST['event']);

            $transport = Swift_SmtpTransport::newInstance('smtp.gmail.com', 465, 'ssl');
            $transport->setUsername("");
            $transport->setPassword("");


            $message = Swift_Message::newInstance();
            $message->setTo(array(
                "@gmail.com" => ""
            ));

            $message->setSubject("This email is sent using Swift Mailer");
            $message->setBody("You're our best client ever.");
            $message->setFrom("account@bank.com", "Your bank");


            $mailer = Swift_Mailer::newInstance($transport);

            if($mailer->send($message)){
                $_SESSION['feedback_positive'][] = "Thank you for your interest!";
                return true;
            }else{
                $_SESSION['feedback_negative'][] = "Error occured, please try again!";
                return false;
            }

        }else{
            $_SESSION['feedback_negative'][] = "Error occured, please try again!";
        }

        return false;

    }

}