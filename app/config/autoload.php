<?php

function autoload($class)
{
    if (file_exists("app/core/" . $class . ".php")) {
        require 'app/core/' . $class . '.php';
    } else {
        exit('The file ' . $class . '.php is missing in the core folder.');
    }
}

spl_autoload_register("autoload");
